function draw_plot(data) {
	maxit = 4;
	for (comp_code in data) {
		if (maxit-- <= 0) break;
		$("#plots").append("<h2>" + comp_code + "</h2>")
		var prices = data[comp_code]["price_seq"]
		var portfolio = data[comp_code]["bought_seq"]
		for (i in portfolio) {
			portfolio[i] = portfolio[i] ? 1 : 0;
		}
		var id = 'plot_' + comp_code
		$("#plots").append($("<p>").attr('id', id))
		var chart = new ApexCharts(document.querySelector("#" + id), {
			chart: {
				type: 'line',
				width: '90%',
				height: 600,
				stacked: false
			},
			series: [{
				name: "prices",
				type: "line",
				data: prices
			}, {
				name: "portfolio",
				type: "column",
				data: portfolio
			}],
			yaxis: [{}, { opposite: true }]
		});
		chart.render();
	}
}

$(document).ready(function () {
	$("#plots").append("<h2>Random simulation</h2>");
	$.getJSON(
		"${last_run}?rand=" + Math.random(),
		draw_plot
	);
	setTimeout(function () {
		$("#plots").append("<h2>Historical simulation</h2>")
		$.getJSON(
			"${last_hist}?rand=" + Math.random(),
			draw_plot
		)
	}, 4000);
})