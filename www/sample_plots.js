$(document).ready(function () {
	$.ajax({
		url: "${get_sample_quotes_url}?job_name=test_model"
	}).done(function (data) {
		for (comp_code in data) {
			$("#plots").append("<h2>" + comp_code + "</h2>")
			id = 'plot_' + comp_code
			$("#plots").append($("<p>").attr('id', id))
			var chart = new ApexCharts(document.querySelector("#" + id), {
				chart: {
					type: 'line',
					width: '90%',
					height: 200
				},
				series: [{
					name: comp_code,
					data: data[comp_code]
				}]
			});
			chart.render();
		}
	})
})