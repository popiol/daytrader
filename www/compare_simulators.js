$(document).ready(function () {
	$.getJSON(
		"${data_url}?rand=" + Math.random(),
		function (data) {
			for (comp_code in data) {
				$("#plots").append("<h2>" + comp_code + "</h2>")
				var container = $('<div class="plot_pair"></div>').appendTo("#plots")
				for (plot_name in data[comp_code]) {
					var plot_data = data[comp_code][plot_name]
					var id = plot_name + '_' + comp_code
					container.append($("<p>").attr('id', id))
					var chart = new ApexCharts(document.querySelector("#" + id), {
						chart: {
							type: 'line',
							width: '90%',
							height: 200
						},
						series: [{
							name: comp_code,
							data: plot_data
						}]
					});
					chart.render();
				}
			}
		}
	)
})