module "sample_plots_html" {
    source = "./www"
    file_name = "sample_plots.html"
    inp = local.common_inputs
}

module "sample_plots_js" {
    source = "./www"
    file_name = "sample_plots.js"
    vars = {
        get_sample_quotes_url = module.get_sample_quotes_api.invoke_url
    }
    inp = local.common_inputs
}

module "compare_simulators_html" {
    source = "./www"
    file_name = "compare_simulators.html"
    inp = local.common_inputs
}

module "compare_simulators_js" {
    source = "./www"
    file_name = "compare_simulators.js"
    vars = {
        data_url = "https://s3.us-east-2.amazonaws.com/${local.common_inputs.bucket_name}/www/compare_plots.json"
    }
    inp = local.common_inputs
}

module "last_run_html" {
    source = "./www"
    file_name = "last_run.html"
    inp = local.common_inputs
}

module "last_run_js" {
    source = "./www"
    file_name = "last_run.js"
    vars = {
        last_run = "https://s3.us-east-2.amazonaws.com/${local.common_inputs.bucket_name}/www/last_run_plots.json"
        last_hist = "https://s3.us-east-2.amazonaws.com/${local.common_inputs.bucket_name}/www/last_hist_plots.json"
    }
    inp = local.common_inputs
}
