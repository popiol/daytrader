import ml_utils
import glue_utils
import numpy as np
import warnings
import os
import random
import math

warnings.filterwarnings("ignore")

settings = glue_utils.Settings(ml_utils.bucket)
naive = settings.map["naive"] if "naive" in settings.map else 0.5
naive2 = random.choices([1, 0], [naive, 1 - naive])[0]
train_hist = settings.map["train_hist"] if "train_hist" in settings.map else 0.5
train_hist2 = random.choices([1, 0], [train_hist * 0.66, 1 - train_hist * 0.66])[0]
print(
    "Naive:",
    naive,
    "Train hist:",
    train_hist,
    "Final:",
    "train_hist" if train_hist2 else ("naive" if naive2 else "random"),
)
max_w = settings.map["max_w"] if "max_w" in settings.map else 0.1
max_c = settings.map["max_c"] if "max_c" in settings.map else 0.05
max_s = settings.map["max_s"] if "max_s" in settings.map else 2
print("Max_w:", max_w, max_c, max_s)

train2_n_steps = (
    10  # settings.map["train2_n_steps"] if "train2_n_steps" in settings.map else 16
)
train2_max_mid_change = (
    -0.03
)  # settings.map["train2_max_mid_change"] if "train2_max_mid_change" in settings.map else -.02
train2_min_last_change = (
    -0.001
)  # settings.map["train2_min_last_change"] if "train2_min_last_change" in settings.map else -.001

scores = settings.map["scores"] if "scores" in settings.map else [[], []]

failure = False
train_on_hist = True if not random.randrange(2) else False
print("train_on_hist:", train_on_hist)

if train_on_hist:
    simulator = glue_utils.HistSimulator(
        ml_utils.db, ml_utils.event_table_name, ml_utils.bucket
    )
else:
    simulator = glue_utils.Simulator(ml_utils.bucket)

n = random.randint(0, 400)
for _ in range(n):
    events = simulator.next()
n = random.randint(30, 500 - n)
hist = []
for _ in range(n):
    events = simulator.next()
    if events is None:
        break
    hist.append(events)

if train_hist2:
    train2_args = (train2_n_steps, train2_max_mid_change)
    print("Train2 args:", *train2_args)
    dev = ml_utils.Agent("current", ml_utils.bucket)
    args = [
        x + math.copysign(max(abs(x), 0.01), x) * random.gauss(0, 0.5)
        for x in train2_args
    ]
    args[0] = max(5, args[0])
    print("Args:", args)
    for events in hist:
        dev.train_init2(events, *args)
    dev.train_init_finish(repeat=10)
    dev.save_as("dev")
elif naive2:
    dev = ml_utils.Agent("current", ml_utils.bucket)
    for events in hist:
        dev.train_init(events)
    dev.train_init_finish(repeat=1)
    dev.save_as("dev")
else:
    best_score = None
    best_dev = None
    for _ in range(6):
        dev = ml_utils.Agent("current", ml_utils.bucket)
        dev.set_max_w(max_w, max_c, max_s)
        n = round(random.uniform(15, 30))
        for events in hist[:n]:
            dev.test(events)
        events = hist[20]
        dev.train(events, repeat=10)
        dev.reset()
        for events in hist[21:70]:
            dev.test(events)
        if best_score is None or dev.get_score() > best_score:  # and dev.n_bought:
            best_dev = dev
            best_score = dev.get_score()
            max_w, max_c, max_s = dev.get_max_w()
    print("---- Best score", best_score, "----")
    if best_dev:
        best_dev.save_as("dev")
    else:
        failure = True

if not failure:
    dev = ml_utils.Agent("dev", ml_utils.bucket)
    current = ml_utils.Agent("current", ml_utils.bucket)
    evaluator = ml_utils.Evaluator()
    if False:
        score1, score2 = evaluator.compare_agents(dev, current, verbose=True, quick=True)
        print("Dev score:", score1, ", Current score:", score2)
    score11, score22 = evaluator.compare_agents(
        dev, current, verbose=True, hist=True, length=100
    )
    print("Dev score:", score11, ", Hist score:", score22)
    if False:
        min_score = min(score1, score11, score1 + score11)
        max_score = max(score1, score11, score1 + score11)
        scores[0].append(min_score)
        scores[1].append(max_score)
        n = math.ceil(len(scores[0]) * 0.5) - 1
        th1 = sorted(scores[0])[n]
        th2 = sorted(scores[1])[n]
        print("Th:", th1, th2)
        if len(scores[0]) > 100:
            scores[0] = scores[0][1:]
            scores[1] = scores[1][1:]
    better = (
        score11 > score22 + .1
        #score1 + score11 > score2 + score22
        #and min_score > th1
        #and max_score > th2
        #and max_score > 0
    )
else:
    better = False

if better:
    dev.save_as("current")

if train_hist2:
    if better:
        train_hist = min(0.9, train_hist + 0.1)
    else:
        train_hist = max(0.1, train_hist - 0.05)
else:
    if naive2 and better:
        naive = min(0.8, naive + 0.1)
        train_hist = max(0.2, train_hist - 0.05)
    elif not naive2 and not better:
        naive = min(0.8, naive + 0.05)
        train_hist = min(0.8, train_hist + 0.02)
    elif naive2 and not better:
        naive = max(0.2, naive - 0.05)
        train_hist = min(0.8, train_hist + 0.02)
    elif not naive2 and better:
        naive = max(0.2, naive - 0.1)
        train_hist = max(0.2, train_hist - 0.05)
settings.map["naive"] = naive
settings.map["train_hist"] = train_hist
settings.map["max_w"] = max_w
settings.map["max_c"] = max_c
settings.map["max_s"] = max_s
settings.map["scores"] = scores
settings.save()

glue_utils.logg("Finish")
print()
