import ml_utils
import glue_utils
import numpy as np
import warnings
import datetime
import random

warnings.filterwarnings("ignore")

train_on_hist = True if not random.randrange(2) else False
if train_on_hist:
    simulator = glue_utils.HistSimulator(
        ml_utils.db, ml_utils.event_table_name, ml_utils.bucket
    )
else:
    simulator = glue_utils.Simulator(ml_utils.bucket)
n = random.randint(0, 400)
for _ in range(n):
    simulator.next()
hist = []
for _ in range(100):
    events = simulator.next()
    if events is None:
        break
    hist.append(events)
recreate = True if not random.randrange(10) else False
initial = ml_utils.Agent("initial2", ml_utils.bucket, ver=2, recreate=recreate)

for events in hist:
    if False and random.randrange(2):
        initial.train_init2(events, 5, -0.01)
        # initial.train_init2(events, 10, -0.03)
        # initial.train_init2(events, 15, -0.05)
    else:
        initial.train_init(events)
initial.train_init_finish()
initial.save_as("initial2")

initial = ml_utils.Agent("initial2", ml_utils.bucket, verbose=False)
current = ml_utils.Agent("current", ml_utils.bucket)
evaluator = ml_utils.Evaluator()
if True: #train_on_hist:
    score1, score2 = evaluator.compare_agents(
        initial, current, quick=True, verbose=True, hist=True, length=100
    )
else:
    score1, score2 = evaluator.compare_agents(
        initial, current, quick=True, verbose=True
    )
print("Initial score:", score1, ", Current score:", score2)
print("Train on hist:", train_on_hist)
if score1 > score2 + .05:
    initial.save_as("current")

glue_utils.logg("Finish")
