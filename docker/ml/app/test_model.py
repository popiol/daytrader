import ml_utils
import glue_utils
import numpy as np
import warnings
import datetime

warnings.filterwarnings("ignore")

current = ml_utils.Agent("current", ml_utils.bucket)
prod = ml_utils.Agent("prod", ml_utils.bucket)
evaluator = ml_utils.Evaluator()
score1, score2 = evaluator.compare_agents(current, prod, verbose=True)
print("Current score:", score1, ", Prod score:", score2)
# current.verbose = True
score11, score22 = evaluator.compare_agents(current, prod, hist=True, verbose=True)
print("Current score:", score11, ", Prod score:", score22)
if (
    score1 > score2 and score11 > score22
    and max(score1, score11) > 0.5
):
    current.save_as("prod")
    current.save_as("prod" + datetime.datetime.today().strftime("%Y%m%d%H%M%S"))

glue_utils.logg("Finish")
print()
