import sys
import boto3
import os
import tensorflow as tf
from tensorflow import keras
import pickle
import math
import numpy as np
import shutil
import random
import datetime
import copy

bucket_name = os.environ["bucket_name"]
event_table_name = os.environ["event_table_name"]
aws_region = os.environ["aws_region"]
temporary = int(os.environ["temporary"])
s3 = boto3.resource("s3")
bucket = s3.Bucket(bucket_name)
db = boto3.resource("dynamodb", region_name=aws_region)
event_table = db.Table(event_table_name)


def s3_download(bucket, obj_key):
    filename = obj_key.split("/")[-1]
    f_in = bucket.Object(obj_key).get()
    with open(filename, "wb") as f_out:
        f_out.write(f_in["Body"].read())


s3_download(bucket, "scripts/glue_utils.py")

import glue_utils


class Agent:
    def __init__(self, agent_name, bucket, verbose=False, recreate=False, ver=1):
        try:
            if recreate:
                raise Exception()
            filename = f"{agent_name}_model.h5"
            obj_key = f"model/{agent_name}_model.h5"
            bucket.download_file(obj_key, filename)
            self.model = keras.models.load_model(filename)
            self.loaded = True
        except:
            if ver == 1:
                print("Create model ver 1")
                in_shape = glue_utils.Event.INPUT_SHAPE
                inputs = keras.layers.Input(shape=in_shape)
                l = keras.layers.LSTM(10)(inputs)
                l = keras.layers.Dropout(0.5)(l)
                l = keras.layers.Dense(10, activation="relu")(l)
                l = keras.layers.Dropout(0.5)(l)
                l = keras.layers.Dense(10, activation="relu")(l)
                l = keras.layers.Dropout(0.5)(l)
            elif ver == 2:
                print("Create model ver 2")
                in_shape = glue_utils.Event.INPUT_SHAPE
                inputs = keras.layers.Input(shape=in_shape)
                l = keras.layers.LSTM(15)(inputs)
                l = keras.layers.Dropout(0.5)(l)
                l = keras.layers.Dense(100, activation="relu")(l)
                l = keras.layers.Dropout(0.5)(l)
                l = keras.layers.Dense(10, activation="relu")(l)
                l = keras.layers.Dropout(0.5)(l)
                l = keras.layers.Dense(10, activation="relu")(l)
                l = keras.layers.Dropout(0.5)(l)
                l = keras.layers.Dense(10, activation="relu")(l)
                l = keras.layers.Dropout(0.5)(l)
            buy_action = keras.layers.Dense(1, activation="sigmoid")(l)
            buy_price = keras.layers.Dense(1, activation="sigmoid")(l)
            sell_price = keras.layers.Dense(1, activation="sigmoid")(l)
            self.model = keras.Model(
                inputs=inputs, outputs=[buy_action, buy_price, sell_price]
            )
            self.model.compile(
                optimizer=tf.keras.optimizers.Nadam(learning_rate=0.001),
                loss="mean_squared_logarithmic_error",
                metrics=["mean_squared_logarithmic_error"],
            )
            self.loaded = False
        self.bucket = bucket
        self.agent_name = agent_name
        self.provision = 0.001
        self.reset()
        self.verbose = verbose
        self.max_w = 1
        self.max_c = 0.1
        self.max_s = 2
        self.ver = ver

    def save(self):
        filename = f"{self.agent_name}_model.h5"
        self.model.save(filename, save_format="h5")
        obj_key = f"model/{self.agent_name}_model.h5"
        self.bucket.upload_file(filename, obj_key)

    def save_as(self, agent_name):
        self.agent_name = agent_name
        self.save()
        return self

    def get_n_ticks(self, comp_code):
        return (
            self.portfolio[comp_code]["n_ticks"] if comp_code in self.portfolio else 0
        )

    def get_inputs(self, event):
        inputs = event.get_inputs()
        return inputs

    def handle_orders(self, event, orders):
        comp_code = event.event["comp_code"]
        quote_dt = event.event["quote_dt"]
        hour = int(quote_dt[11:13])
        transaction_made = False
        if 9 <= hour <= 16 and comp_code in self.orders:
            if self.orders[comp_code]["buy"] and self.orders[comp_code][
                "price"
            ] > float(event.get_price()):
                self.portfolio[comp_code] = self.orders[comp_code]
                self.portfolio[comp_code]["n_ticks"] = 1
                self.portfolio[comp_code]["price_init"] = self.portfolio[comp_code][
                    "price"
                ]
                self.cash -= (
                    self.portfolio[comp_code]["n_shares"]
                    * self.orders[comp_code]["price"]
                    * (1 + self.provision)
                )
                transaction_made = True
                self.n_bought += 1
                event.just_bought = True

                self.bought_comps.append({"comp": comp_code, "tick": self.n_ticks})

                if self.verbose:
                    print(event.event["quote_dt"])
                    print(
                        "Buy",
                        self.portfolio[comp_code]["n_shares"],
                        "shares of",
                        comp_code,
                        "for",
                        self.orders[comp_code]["price"],
                    )
                    print("Cash:", self.cash, ", Capital:", self.get_capital())
            elif not self.orders[comp_code]["buy"] and self.orders[comp_code][
                "price"
            ] < float(event.get_price()):
                del self.portfolio[comp_code]
                self.cash += (
                    self.orders[comp_code]["n_shares"]
                    * self.orders[comp_code]["price"]
                    * (1 - self.provision)
                )
                transaction_made = True
                self.n_sold += 1
                event.just_sold = True
                if self.verbose:
                    print(event.event["quote_dt"])
                    print(
                        "Sell",
                        self.orders[comp_code]["n_shares"],
                        "shares of",
                        comp_code,
                        "for",
                        self.orders[comp_code]["price"],
                    )
                    print("Cash:", self.cash, ", Capital:", self.get_capital())
        if transaction_made and comp_code in orders:
            del orders[comp_code]

    def add_sell_order(self, event, sell_price_ch, orders):
        comp_code = event.event["comp_code"]
        price = event.get_price()
        if (
            comp_code in self.portfolio
            and comp_code not in orders
            and abs(sell_price_ch) < 0.1
        ):
            self.portfolio[comp_code]["price"] = price
            sell_price = round(price * (1 + sell_price_ch), 2)
            if self.verbose:
                print("Sell price:", comp_code, price, sell_price)
            orders[comp_code] = {
                "buy": False,
                "price": sell_price,
                "n_shares": self.portfolio[comp_code]["n_shares"],
            }

    def get_capital(self):
        capital = sum(
            self.portfolio[x]["n_shares"] * self.portfolio[x]["price"]
            for x in self.portfolio
        )
        capital += self.cash
        return capital

    def get_stats(self, events, prev_events, verbose=True):
        ups = 0
        downs = 0
        price_ch = []
        single_ch = []
        prev_events2 = {}
        for event in prev_events:
            comp_code = event.event["comp_code"]
            prev_events2[comp_code] = event
        for event in events:
            comp_code = event.event["comp_code"]
            if comp_code in self.first_events:
                if event.get_price() > self.first_events[comp_code].get_price():
                    ups += 1
                elif event.get_price() < self.first_events[comp_code].get_price():
                    downs += 1
                price_ch.append(
                    event.get_price() / self.first_events[comp_code].get_price() - 1
                )
            if comp_code in prev_events2:
                single_ch.append(
                    event.get_price() / prev_events2[comp_code].get_price() - 1
                )
        ratio = ups / (ups + downs)
        price_daily = []
        for comp_code in self.daily:
            prev_price = self.daily[comp_code][0]
            for price in self.daily[comp_code][1:]:
                ch = price / prev_price - 1
                prev_price = price
                price_daily.append(ch)
        if verbose:
            print(
                "Ups:",
                ups,
                ", downs:",
                downs,
                ", ratio:",
                ratio,
                ", avg:",
                np.average(price_ch),
                ", daily std:",
                np.std(price_daily),
            )
            print(
                "Histogram:",
                np.histogram(
                    single_ch, [-1] + np.linspace(-0.05, 0.05, 11).tolist() + [1]
                )[0],
            )
        return ratio

    def next(self, events, get_outputs):
        inputs = []
        outputs2 = []
        comp_codes = []
        best_event = None
        worst_event = None
        orders = {}
        for event in events:
            inputs.append(self.get_inputs(event))
        outputs = get_outputs(events, inputs)
        prev_events = {}
        for event in events:
            comp_code = event.event["comp_code"]
            quote_dt = event.event["quote_dt"]
            stale = event.event["stale"] if "stale" in event.event else False
            hour = int(quote_dt[11:13])
            prev_events[comp_code] = event
            if hour == 16:
                if comp_code in self.daily:
                    self.daily[comp_code].append(event.get_price())
                else:
                    self.daily[comp_code] = [event.get_price()]
            if self.prev_events is not None and comp_code in self.prev_events:
                price_ch = (
                    event.get_price() / self.prev_events[comp_code].get_price() - 1
                )
                if price_ch > 0:
                    self.price_up.append(price_ch)
                elif price_ch < 0:
                    self.price_down.append(price_ch)
            if (
                "old_comp_code" in event.event
                and event.event["old_comp_code"] in self.portfolio
            ):
                old_comp_code = event.event["old_comp_code"]
                self.portfolio[comp_code] = self.portfolio[old_comp_code]
                del self.portfolio[old_comp_code]
            self.handle_orders(event, orders)
            buy_action, buy_price, sell_price = outputs[comp_code]
            buy_action = min(0.6, buy_action)
            buy_price /= 50
            sell_price /= 20
            if (
                (best_event is None or buy_action > best_buy)
                and comp_code not in self.portfolio
                and abs(buy_price) < 0.1
                and not stale
            ):
                best_event = event
                best_buy = buy_action
                best_price = round(event.get_price() * (1 + buy_price), 2)
            if (
                (worst_event is None or buy_action < worst_buy)
                and comp_code not in self.portfolio
                and abs(buy_price) < 0.1
                and not stale
            ):
                worst_event = event
                worst_buy = buy_action
                worst_price = round(event.get_price() * (1 + buy_price), 2)
            outputs1 = [buy_action, buy_price, sell_price]
            outputs1 = [(x + 1) / 2 for x in outputs1]
            outputs2.append(outputs1)
            comp_codes.append(comp_code)
            self.add_sell_order(event, sell_price, orders)
        self.prev_events = prev_events
        if self.first_events is None:
            self.first_events = prev_events
        if self.verbose and best_event is not None:
            print(
                "Best buy:",
                best_event.event["comp_code"],
                best_buy,
                best_event.get_price(),
                best_price,
            )
            print(
                "Worst buy:",
                worst_event.event["comp_code"],
                worst_buy,
                worst_event.get_price(),
                worst_price,
            )
        self.orders = orders
        n_buys = sum(1 if self.orders[x]["buy"] else 0 for x in self.orders)
        if self.cash > 200 and best_event is not None and n_buys == 0:
            capital = self.get_capital()
            n = math.floor(capital / best_price * best_buy * (1 - self.provision))
            n = min(n, math.floor(self.cash / best_price * (1 - self.provision)))
            if n > 0:
                comp_code = best_event.event["comp_code"]
                self.orders[comp_code] = {
                    "buy": True,
                    "price": best_price,
                    "n_shares": n,
                }
        for comp_code in self.portfolio:
            self.portfolio[comp_code]["n_ticks"] += 1
        self.n_ticks += 1
        return inputs, outputs2, comp_codes

    def fit(self, x, y, repeat=100):
        x = np.array(x)
        y = list(zip(*y))
        y = [np.array(x) for x in y]
        with open("/dev/null", "w") as f:
            sys.stdout = f
            # self.model.fit(x, y)
            for _ in range(repeat):
                self.model.train_on_batch(x, y)
            sys.stdout = sys.__stdout__

    def train_init(self, events):
        events2 = {}
        quote_dt = events[0].event["quote_dt"]
        for event in events:
            comp_code = event.event["comp_code"]
            events2[comp_code] = event
        self.event_hist.append(events2)
        if len(self.event_hist) > 10:
            del self.event_hist[0]
        best_buy = None
        worst_buy = None
        n_good = 0
        n_bad = 0
        for event in events:
            if len(self.event_hist) < 10:
                break
            comp_code = event.event["comp_code"]
            if comp_code not in self.event_hist[0]:
                continue
            first_event = self.event_hist[0][comp_code]
            min_gain1 = None
            max_gain = None
            for prev_event in self.event_hist[1:]:
                if comp_code not in prev_event:
                    continue
                gain = prev_event[comp_code].get_price() / first_event.get_price() - 1
                if min_gain1 is None:
                    min_gain1 = gain
                    min_gain0 = gain
                else:
                    if max_gain is None or gain > max_gain:
                        max_gain = gain
                        min_gain2 = min_gain1
                        min_gain3 = gain
                    if gain < min_gain1:
                        min_gain1 = gain
                    if gain < min_gain3:
                        min_gain3 = gain
            if max_gain is not None:
                if max_gain > -min_gain1 and n_good <= n_bad:
                    n_good += 1
                elif max_gain < -min_gain1 and n_good >= n_bad:
                    n_bad += 1
                else:
                    continue

                inputs1 = self.get_inputs(first_event)
                buy_action = 10 * min(0.03, max_gain + 8 * min_gain1) - 10 * max(
                    -0.01, min_gain0
                )
                buy_action = buy_action / (1 + abs(buy_action)) + 0.23
                buy_price = max(
                    -1, min(0.001 * 50, (max(-0.01, min_gain0) + 0.001 + 0.002) * 50)
                )
                sell_price = min(1, max(-0.001 * 20, (max_gain - 0.001) * 20))

                n_ticks = int(min(len(inputs1), max(0, round(7 - buy_action * 70))))
                for i in range(n_ticks):
                    inputs1[-i - 1][1] = 1

                buy_action = max(-1, min(1, buy_action))
                output1 = [buy_action, buy_price, sell_price]
                output1 = [(x + 1) / 2 for x in output1]
                if comp_code in self.inputs:
                    self.inputs[comp_code].append(inputs1)
                    self.outputs[comp_code].append(output1)
                else:
                    self.inputs[comp_code] = [inputs1]
                    self.outputs[comp_code] = [output1]
                if best_buy is None or buy_action > best_buy:
                    best_buy = buy_action
                    best_comp_code = comp_code
                    best_buy_price = buy_price
                    best_sell_price = sell_price
                if worst_buy is None or buy_action < worst_buy:
                    worst_buy = buy_action
                    worst_comp_code = comp_code
                    worst_buy_price = buy_price
                    worst_sell_price = sell_price
        if self.verbose and best_buy is not None:
            print(
                quote_dt,
                self.n_ticks,
                best_comp_code,
                best_buy,
                best_buy_price,
                best_sell_price,
            )
            print(
                quote_dt,
                self.n_ticks,
                worst_comp_code,
                worst_buy,
                worst_buy_price,
                worst_sell_price,
            )
        self.n_ticks += 1

    def train_init_finish(self, repeat=100):
        if self.inputs:
            inputs = {"batch": [b for a in list(self.inputs.values()) for b in a]}
            outputs = {"batch": [b for a in list(self.outputs.values()) for b in a]}
            print("Trainset size:", len(inputs["batch"]))
            if len(inputs["batch"]) > 10000:
                inputs = self.inputs
                outputs = self.outputs
            repeat = round(math.sqrt(repeat))
            for _ in range(repeat):
                for batch in inputs:
                    if len(inputs[batch]) > 10:
                        self.fit(inputs[batch], outputs[batch], repeat=repeat)

    def train_init2(self, events, n_steps, max_change):
        n_steps = round(n_steps)
        events2 = {}
        for event in events:
            comp_code = event.event["comp_code"]
            events2[comp_code] = event
        self.event_hist.append(events2)
        if len(self.event_hist) > n_steps:
            del self.event_hist[0]
        n_good = 0
        n_bad = 0
        for event in events:
            if len(self.event_hist) < n_steps:
                break
            comp_code = event.event["comp_code"]
            try:
                first_event = self.event_hist[0][comp_code]
                last_event = self.event_hist[-2][comp_code]
                next_event = self.event_hist[-1][comp_code]
                mid_event = self.event_hist[-round(7 / 12 * n_steps)][comp_code]
            except:
                continue
            whole_change = last_event.get_price() / first_event.get_price() - 1
            next_change = next_event.get_price() / last_event.get_price() - 1
            mid_change = last_event.get_price() / mid_event.get_price() - 1

            buy_price = next_change + 0.001
            sell_price = max(-0.001, -whole_change - 0.01)

            if whole_change < max_change and abs(mid_change) < 0.01:
                buy_action = min(1, 0.8 - 10 * buy_price)
                if next_change < buy_price:
                    next_event.bought_seq[-1] = 1
            else:
                buy_action = -0.2
                if next_change > sell_price:
                    next_event.bought_seq[-1] = 0

            buy_price = max(-1, buy_price * 50)
            sell_price = min(1, sell_price * 20)

            if buy_action > 0 and n_good <= n_bad:
                n_good += 1
            elif buy_action < 0 and n_good >= n_bad:
                n_bad += 1
            else:
                continue

            inputs1 = self.get_inputs(last_event)
            output1 = [buy_action, buy_price, sell_price]
            output1 = [(x + 1) / 2 for x in output1]

            if comp_code in self.inputs:
                self.inputs[comp_code].append(inputs1)
                self.outputs[comp_code].append(output1)
            else:
                self.inputs[comp_code] = [inputs1]
                self.outputs[comp_code] = [output1]

    def reset(self):
        self.score = 0
        self.active_score = 0
        self.min_weekly = None
        self.weekly_ticks = 0
        self.n_ticks = 0
        self.week_start_val = None
        self.portfolio = {}
        self.orders = {}
        self.cash = 1000
        self.event_hist = []
        self.n_bought = 0
        self.n_sold = 0
        self.price_up = []
        self.price_down = []
        self.daily = {}
        self.prev_events = None
        self.first_events = None
        self.version = None
        self.bought_comps = []
        self.inputs = {}
        self.outputs = {}

    def get_test_outputs(self, events, inputs):
        outputs = self.model.predict(inputs)
        outputs = list(zip(*outputs))
        outputs = [[y[0] * 2 - 1 for y in x] for x in outputs]
        outputs = {event.event["comp_code"]: out for event, out in zip(events, outputs)}
        return outputs

    def test(self, events):
        week_n_ticks = 40
        prev_capital = self.get_capital()
        self.next(events, self.get_test_outputs)
        capital = self.get_capital()
        score = capital / prev_capital - 1
        if self.weekly_ticks == 0:
            if self.week_start_val is not None:
                weekly_val = capital / self.week_start_val - 1
                if self.min_weekly is None or weekly_val < self.min_weekly:
                    self.min_weekly = weekly_val
                score += self.min_weekly / week_n_ticks
            self.week_start_val = capital
        self.weekly_ticks = (self.weekly_ticks + 1) % week_n_ticks
        if self.active_score > 0.8:
            active_score = 0
        else:
            active_score = self.n_sold / (self.n_ticks // 7 + 1) / 200
        self.active_score += active_score
        self.score += score + active_score

    def set_max_w(self, max_w, max_c, max_s):
        self.max_w = max_w
        self.max_c = max_c
        self.max_s = max_s

    def get_max_w(self):
        return self.max_w, self.max_c, self.max_s

    def get_train_outputs(self, events, inputs):
        outputs = self.get_test_outputs(events, inputs)
        self.grad = []
        grad_base = None
        for event_i, event in enumerate(events):
            comp_code = event.event["comp_code"]
            input1 = inputs[event_i]
            input1 = np.array(input1).flatten("F").tolist()
            if grad_base is None:
                grad_base = [
                    [random.uniform(-self.max_w, self.max_w) for y in input1]
                    + [random.uniform(-self.max_c, self.max_c)]
                    for x in outputs[comp_code]
                ]
                sign = [
                    random.uniform(-self.max_s, self.max_s) for x in outputs[comp_code]
                ]
            grad = [
                sum(
                    (input1[xi] if xi < len(input1) else 1) * x
                    for xi, x in enumerate(grad_base[oi])
                )
                for oi in range(len(grad_base))
            ]
            outputs[comp_code] = [
                min(1, max(-1, s * (x + y)))
                for x, y, s in zip(outputs[comp_code], grad, sign)
            ]
            self.grad.append(grad)
        self.sign = sign
        max_w = np.average(np.absolute([x[:-1] for x in grad_base]))
        max_c = np.average(np.absolute([x[-1] for x in grad_base]))
        max_s = np.average(np.absolute(sign))
        self.max_w = (9 * self.max_w + 2 * max_w) / 10
        self.max_c = (9 * self.max_c + 2 * max_c) / 10
        self.max_s = (9 * self.max_s + 2 * max_s) / 10
        return outputs

    def train(self, events, repeat=1):
        inputs, outputs, comp_codes = self.next(events, self.get_train_outputs)
        self.fit(inputs, outputs, repeat)

    def explore(self, events):
        inputs, outputs, comp_codes = self.next(events, self.get_test_outputs)
        if "batch" not in self.inputs:
            self.inputs["batch"] = []
            self.outputs["batch"] = []
        buy_comp = None
        sell_comp = None
        for comp_code, order in self.orders.items():
            if order["buy"]:
                buy_comp = comp_code
            else:
                sell_comp = comp_code
        if buy_comp:
            comp_i = comp_codes.index(buy_comp)
            self.inputs["batch"].append(inputs[comp_i])
            outputs[comp_i][0] += random.gauss(0, 0.01)
            outputs[comp_i][1] += random.gauss(0, 0.002)
            self.outputs["batch"].append(outputs[comp_i])
        if sell_comp:
            comp_i = comp_codes.index(sell_comp)
            self.inputs["batch"].append(inputs[comp_i])
            outputs[comp_i][2] += random.gauss(0, 0.002)
            self.outputs["batch"].append(outputs[comp_i])

    def learn_from_mistakes(self, events):
        inputs, outputs, comp_codes = self.next(events, self.get_test_outputs)
        if "batch" not in self.inputs:
            self.inputs["batch"] = []
            self.outputs["batch"] = []
        if len(self.event_hist) >= 2:
            last_event = self.event_hist[-2]
            for comp_code in last_event:
                if comp_code not in self.event_hist[-1]:
                    for event in self.event_hist:
                        if comp_code in event:
                            first_event = event
                            break
                    if last_event[comp_code]["gain"] > 0.02:
                        if first_event is last_event:
                            events2 = [first_event]
                        else:
                            events2 = [first_event, last_event]
                        for event in events2:
                            self.inputs["batch"].append(event[comp_code]["inputs"])
                            self.outputs["batch"].append(event[comp_code]["outputs"])
                    else:
                        first_event[comp_code]["outputs"][0] = 0
                        self.inputs["batch"].append(first_event[comp_code]["inputs"])
                        self.outputs["batch"].append(first_event[comp_code]["outputs"])

        events2 = {}
        for comp_code, order in self.orders.items():
            if order["buy"]:
                comp_i = comp_codes.index(comp_code)
                events2[comp_code] = {
                    "buy": True,
                    "gain": 0,
                    "inputs": inputs[comp_i],
                    "outputs": outputs[comp_i],
                }
        for comp_code, item in self.portfolio.items():
            comp_i = comp_codes.index(comp_code)
            gain = item["price"] / item["price_init"] - 1
            events2[comp_code] = {
                "buy": False,
                "gain": gain,
                "inputs": inputs[comp_i],
                "outputs": outputs[comp_i],
            }
        if events2:
            self.event_hist.append(events2)
            if len(self.event_hist) > 10:
                del self.event_hist[0]

    def get_score(self):
        return self.score + (self.get_capital() - 1000) / 100


class Evaluator:
    def __init__(self):
        self.random_avg = None
        self.random_std = None

    def compare_agents(
        self, agent1, agent2, hist=False, quick=False, verbose=False, length=None
    ):
        scores1 = []
        scores2 = []
        offset_range = [-1] if hist else ([0] if quick else [0, -1, 1])
        for offseti in range(len(offset_range)):
            offset = offset_range[offseti]
            max_it = 1  # 1 if hist else (1 if quick else 5)
            for _ in range(max_it):
                if hist:
                    simulator = glue_utils.HistSimulator(db, event_table_name, bucket)
                else:
                    simulator = glue_utils.Simulator(bucket, offset)
                agent1.reset()
                agent2.reset()
                quote_dt = None
                if length:
                    max_steps = length
                else:
                    max_steps = 1000 if hist else 100
                n = random.randrange(max(1, (400 - max_steps)))
                for _ in range(n):
                    simulator.next()
                prev_events = None
                events3 = {}
                events1 = None
                for step in range(max_steps):
                    prev_prev_events = prev_events
                    prev_events = events1
                    events1 = simulator.next()
                    if events1 is None:
                        print("Stopping after", step, "iterations, quote_dt:", quote_dt)
                        events1 = prev_events
                        prev_events = prev_prev_events
                        break
                    events2 = copy.deepcopy(events1)
                    for event in events2:
                        comp_code = event.event["comp_code"]
                        if comp_code in events3:
                            event.bought_seq = events3[comp_code]
                    quote_dt = events1[0].event["quote_dt"]
                    agent1.test(events1)
                    agent2.test(events2)
                    for event in events2:
                        comp_code = event.event["comp_code"]
                        last = event.bought_seq[-1]
                        just_bought = event.just_bought
                        just_sold = event.just_sold
                        event.bought_seq.append(
                            1 if (last and not just_sold) or just_bought else 0
                        )
                        events3[comp_code] = event.bought_seq
                events = events1
                scores1.append(agent1.get_score())
                scores2.append(agent2.get_score())
                print("Capital:", agent1.get_capital(), agent2.get_capital())
                print(
                    "Bought/Sold:",
                    agent1.n_bought,
                    "/",
                    agent1.n_sold,
                    "-",
                    agent2.n_bought,
                    "/",
                    agent2.n_sold,
                )
            # print(agent1.bought_comps)
            # if not hist:
            #    simulator.print_sample_quotes([x['comp'] for x in agent1.bought_comps])
            try:
                ratio = agent1.get_stats(events, prev_events, verbose=verbose)
            except:
                simulator.revert_adjustment()
                break
            change_avg = (
                np.average(agent1.price_up) + abs(np.average(agent1.price_down))
            ) / 2
            change_std = (np.std(agent1.price_up) + abs(np.std(agent1.price_down))) / 2
            change_avg_up = np.average(agent1.price_up)
            change_avg_down = abs(np.average(agent1.price_down))
            if not hist:
                if offset == 0:
                    scale_offset = None
                    self.random_avg = change_avg
                    self.random_std = change_std
                    self.random_avg_up = change_avg_up
                    self.random_avg_down = change_avg_down
                    scale_power = None
                    scale_c_up = None
                    scale_c_down = None
                    scale_shift = round((0.5 - ratio) * 10) / 2
                else:
                    if offset > 0:
                        scale_offset = round((0.7 - ratio) * 10) / 2
                    else:
                        scale_offset = -round((0.4 - ratio) * 10) / 2
                    scale_power = None
                    scale_c_up = None
                    scale_c_down = None
                    scale_shift = None
                simulator.adjust(
                    scale_offset, scale_power, scale_c_up, scale_c_down, scale_shift
                )
            elif self.random_avg is not None:
                scale_offset = None
                hist_avg = change_avg
                hist_std = change_std
                scale_c_up = round((change_avg_up - self.random_avg_up) * 1000) / 10
                scale_c_down = (
                    round((change_avg_down - self.random_avg_down) * 1000) / 10
                )
                scale_power = (
                    round(
                        (hist_std - hist_avg - self.random_std + self.random_avg) * 200
                    )
                    / 10
                    / (abs(scale_c_up) * 5 + 0.5)
                )
                scale_shift = None
                glue_utils.PriceChModel(bucket).adjust(
                    scale_offset, scale_power, scale_c_up, scale_c_down, scale_shift
                )
            if offseti == 0 and len(offset_range) > 1:
                if ratio > 0.6:
                    offset_range = [0, -1, -2]
                elif ratio < 0.4:
                    offset_range = [0, 1, 2]
            if verbose:
                print(
                    "Price up/down:",
                    np.average(agent1.price_up),
                    "/",
                    np.average(agent1.price_down),
                    np.std(agent1.price_up),
                    "/",
                    np.std(agent1.price_down),
                )
            if offseti == 0:
                simulator.save_plots()
        score1 = np.average(scores1)  # + min(scores1) / 3
        score2 = np.average(scores2)  # + min(scores2) / 3
        return score1, score2
