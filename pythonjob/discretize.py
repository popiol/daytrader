import sys
from awsglue.utils import getResolvedOptions
import boto3
from boto3.dynamodb.conditions import Key, Attr
import json
import glue_utils
from sklearn.preprocessing import KBinsDiscretizer
import pickle
import numpy as np
import random
from decimal import Decimal

# get params
args = getResolvedOptions(sys.argv, ["bucket_name", "alert_topic", "log_table", "event_table", "app", "temporary"])
bucket_name = args["bucket_name"]
alert_topic = args["alert_topic"]
log_table_name = args["log_table"]
event_table_name = args["event_table"]
app = json.loads(args["app"])
temporary = True if args["temporary"] == "true" or args["temporary"] == "1" else False
db = boto3.resource("dynamodb")
event_table = db.Table(event_table_name)
s3 = boto3.resource("s3")
bucket = s3.Bucket(bucket_name)

# get list of all company codes
comp_codes, quote_dts = glue_utils.list_companies(event_table)

# alert if input files missing
if not comp_codes:
    sns = boto3.resource("sns")
    topic = sns.Topic(alert_topic)
    topic.publish(Message="Missing files in events/")
    print("Missing files in events/")
    exit()

# create list of price changes
price_chs = []
for comp_code in comp_codes:
    res = event_table.query(KeyConditionExpression=Key("comp_code").eq(comp_code))

    if not res["Items"]:
        continue

    price = None
    for item in res["Items"]:
        quote_dt = item["quote_dt"]
        prev_price = price
        event = glue_utils.OldEvent(bucket=bucket, event_table=event_table, comp_code=comp_code, quote_dt=quote_dt)
        price = event.get_price()
        if prev_price is None:
            continue
        if prev_price < 0.01 or price < 0.01:
            continue
        price_ch = price / prev_price - 1
        if price_ch < -0.1 or price_ch > 0.1:
            continue
        if random.randrange(100):
            continue

        price_chs.append(price_ch)

if not price_chs:
    if temporary:
        price_chs = [0, 0.01, -0.01]
    else:
        print("No price changes")
        exit()

if temporary:
    for _ in range(10):
        price_chs.append(random.gauss(0, 0.005))

# discretize
discretizer = KBinsDiscretizer(
    n_bins=glue_utils.PRICE_CHANGE_N_BINS,
    encode="ordinal",
)
X = np.array(price_chs)
X = X[abs(X - np.mean(X)) < 2 * np.std(X)]
X = np.reshape(X, (-1, 1))
discretizer.fit(X)
print(discretizer.bin_edges_)

# save discretizer
discretizer = glue_utils.Discretizer(discretizer=discretizer)
discretizer.save(bucket)
