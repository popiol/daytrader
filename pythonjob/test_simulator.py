import sys
import datetime
from awsglue.utils import getResolvedOptions
import boto3
import csv
import io
from boto3.dynamodb.conditions import Key, Attr
import json
import glue_utils
import random

#get params
args = getResolvedOptions(sys.argv, ['bucket_name','alert_topic','log_table','event_table','app','temporary'])
bucket_name = args['bucket_name']
alert_topic = args['alert_topic']
log_table_name = args['log_table']
event_table_name = args['event_table']
app = json.loads(args['app'])
temporary = True if args['temporary'] == "true" or args['temporary'] == "1" else False

s3 = boto3.resource("s3")
bucket = s3.Bucket(bucket_name)
db = boto3.resource('dynamodb')

def add_to_plots(plots, events, label):
    for event in events:
        comp_code = event.event['comp_code']
        add = 1 if comp_code in plots else (0 if len(plots) > 20 else random.choices([0,1],[30,1])[0])
        if add:
            plot = plots[comp_code] if comp_code in plots else {}
            plot2 = plot[label] if label in plot else []
            plot2.append(event.get_price())
            plot[label] = plot2
            plots[comp_code] = plot

hist_simulator = glue_utils.HistSimulator(db, event_table_name, bucket)
plots = {}

for _ in range(100):
    events = hist_simulator.next()
    add_to_plots(plots, events, 'hist')
    add_to_plots(plots, events, 'rand')

simulator = glue_utils.Simulator(bucket, events=events)

for _ in range(100):
    events = hist_simulator.next()
    if events is None:
        break
    add_to_plots(plots, events, 'hist')
    events = simulator.next()
    add_to_plots(plots, events, 'rand')

obj_key = 'www/compare_plots.json'
plots = json.dumps(plots)
bucket.put_object(Key=obj_key, Body=plots, ACL="public-read")
