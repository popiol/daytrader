import sys
from awsglue.utils import getResolvedOptions
import boto3
from boto3.dynamodb.conditions import Key, Attr
import json
import glue_utils
from sklearn.preprocessing import KBinsDiscretizer
import pickle
from sklearn.neural_network import MLPClassifier
import datetime
import numpy as np
import random
import math

# get params
args = getResolvedOptions(sys.argv, ["bucket_name", "alert_topic", "log_table", "event_table", "app", "temporary"])
bucket_name = args["bucket_name"]
alert_topic = args["alert_topic"]
log_table_name = args["log_table"]
event_table_name = args["event_table"]
app = json.loads(args["app"])
temporary = True if args["temporary"] == "true" or args["temporary"] == "1" else False
s3 = boto3.resource("s3")
bucket = s3.Bucket(bucket_name)
db = boto3.resource("dynamodb")
event_table = db.Table(event_table_name)

# create model
try:
    model = glue_utils.PriceChModel(bucket)
except:
    model = MLPClassifier(warm_start=True)
    model = glue_utils.PriceChModel(model=model)

# get list of all company codes
comp_codes, quote_dts = glue_utils.list_companies(event_table)

# alert if input files missing
if not comp_codes:
    sns = boto3.resource("sns")
    topic = sns.Topic(alert_topic)
    topic.publish(Message="Missing events")
    print("Missing events")
    exit()

# get bins
discretizer = glue_utils.Discretizer(bucket)

# build model
start_dt = datetime.datetime.now()
start_dt -= datetime.timedelta(days=7)
start_dt = start_dt.strftime(glue_utils.DB_DATE_FORMAT)
all_classes = []
trainset = {}
for comp_code in comp_codes:
    res = event_table.query(
        KeyConditionExpression=Key("comp_code").eq(comp_code) & Key("quote_dt").gt("2020-06-01 00:00:00")
    )

    if not res["Items"]:
        continue

    event = None
    train_x = []
    train_y = []
    for item in res["Items"]:
        quote_dt = item["quote_dt"]
        prev_event = event
        event = glue_utils.OldEvent(bucket=bucket, event_table=event_table, comp_code=comp_code, quote_dt=quote_dt)
        if prev_event is None:
            continue
        price1 = prev_event.get_price()
        price2 = event.get_price()
        if price1 < 0.01 or price2 < 0.01:
            continue
        price_ch = price2 / price1 - 1
        if price_ch < -0.1 or price_ch > 0.1:
            continue
        if random.randrange(100):
            continue
        inputs = event.get_inputs()
        price_class = discretizer.price_class(price_ch)
        train_x.append(inputs)
        train_y.append(price_class)
        all_classes.append(price_class)
    if train_x and train_y:
        trainset[comp_code] = [train_x, train_y]

n_bins = glue_utils.PRICE_CHANGE_N_BINS
hist, _ = np.histogram(all_classes, n_bins)
print(hist)
expected = np.repeat(len(all_classes) / n_bins, n_bins)
probs = expected / hist

all_classes = []
all_inputs = []
for comp_code, item in trainset.items():
    for x, y in zip(item[0], item[1]):
        p = probs[y]
        if math.isinf(p):
            continue
        n = int(round(random.uniform(0, p * 2)))
        for _ in range(n):
            all_inputs.append(x)
            all_classes.append(y)

hist, edges = np.histogram(all_classes, n_bins)
print(hist)

model.means = np.mean(all_inputs, axis=0)
model.stds = np.std(all_inputs, axis=0)

model.fit(all_inputs, all_classes)

# save model
model.save(bucket)
