import sys
from awsglue.utils import getResolvedOptions
import boto3
from boto3.dynamodb.conditions import Key, Attr
import json
import glue_utils
import pickle
from sklearn.neural_network import MLPRegressor
import datetime
import numpy as np
import math
import random

# get params
args = getResolvedOptions(
    sys.argv,
    ["bucket_name", "alert_topic", "log_table", "event_table", "app", "temporary"],
)
bucket_name = args["bucket_name"]
alert_topic = args["alert_topic"]
log_table_name = args["log_table"]
event_table_name = args["event_table"]
app = json.loads(args["app"])
temporary = True if args["temporary"] == "true" or args["temporary"] == "1" else False
s3 = boto3.resource("s3")
bucket = s3.Bucket(bucket_name)
db = boto3.resource("dynamodb")
event_table = db.Table(event_table_name)

# create model
try:
    model = glue_utils.PriceChModel(bucket)
except:
    model = MLPRegressor(warm_start=True)
    model = glue_utils.PriceChModel(model=model)

# get list of all company codes
comp_codes, quote_dts = glue_utils.list_companies(event_table)
print("comp list:", len(comp_codes))

# alert if input files missing
if not comp_codes:
    sns = boto3.resource("sns")
    topic = sns.Topic(alert_topic)
    topic.publish(Message="Missing events")
    print("Missing events")
    exit()

# build model
start_dt = datetime.datetime.now()
start_dt -= datetime.timedelta(days=7)
start_dt = start_dt.strftime(glue_utils.DB_DATE_FORMAT)
price_chs = []
trainset = {}
for comp_code in comp_codes:
    res = event_table.query(
        KeyConditionExpression=Key("comp_code").eq(comp_code)
        & Key("quote_dt").gt("2020-06-01 00:00:00")
    )

    if not res["Items"]:
        continue

    event = None
    train_x = []
    train_y = []
    for item in res["Items"]:
        quote_dt = item["quote_dt"]
        prev_event = event
        event = glue_utils.OldEvent(
            bucket=bucket,
            event_table=event_table,
            comp_code=comp_code,
            quote_dt=quote_dt,
        )
        if prev_event is None:
            continue
        price1 = prev_event.get_price()
        price2 = event.get_price()
        if price1 < 0.01 or price2 < 0.01:
            continue
        price_ch = price2 / price1 - 1
        if price_ch < -0.1 or price_ch > 0.1:
            continue
        if random.randrange(100):
            continue
        inputs = event.get_inputs()
        train_x.append(inputs)
        train_y.append(price_ch)
        price_chs.append(price_ch)

    if train_x and train_y:
        trainset[comp_code] = [train_x, train_y]

n_bins = 10
bins = np.linspace(-0.01, 0.01, n_bins)
edges = [(x + bins[xi + 1]) / 2 for xi, x in enumerate(bins[:-1])]
edges = [min(-0.05, min(price_chs))] + edges + [max(0.05, max(price_chs))]
print(edges)
hist, edges = np.histogram(price_chs, bins=edges)
print(hist)
bins = [
    x if abs(x) < abs(edges[xi + 1]) else edges[xi + 1]
    for xi, x in enumerate(edges[:-1])
]
gauss = [math.exp(-(x ** 2) / (2 * 0.002 ** 2)) for x in bins]
gauss = np.array(gauss) * len(price_chs) / sum(gauss)
np.seterr(divide="ignore")
probs = gauss / hist

trainset2 = [[], []]
price_chs = []
for comp_code in trainset:
    train_x = []
    train_y = []
    ps = probs[np.digitize(trainset[comp_code][1], edges[1:-1])]
    for pi, p in enumerate(ps):
        n = int(round(random.uniform(0, p * 2)))
        for _ in range(n):
            train_x.append(trainset[comp_code][0][pi])
            train_y.append(trainset[comp_code][1][pi])
            price_chs.append(train_y[-1])
    if train_x and train_y:
        trainset2[0].extend(train_x)
        trainset2[1].extend(train_y)

hist, edges = np.histogram(price_chs, bins=edges)
print(hist)

model.means = np.mean(trainset2[0], axis=0)
model.stds = np.std(trainset2[0], axis=0)

model.fit(trainset2[0], trainset2[1])

# save model
model.save(bucket)
