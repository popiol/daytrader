import pickle
import random
import math
import numpy as np
import datetime
import json
import boto3
from boto3.dynamodb.conditions import Key, Attr
from decimal import Decimal
import time
import sys

PRICE_CHANGE_N_BINS = 10
DB_DATE_FORMAT = "%Y-%m-%d %H:%M:%S"
SIM_N_COMPS = 450
N_VANISHING_COMPS = 100
MIN_EVENTS_LEN = 100


def logg(x):
    print("---- [{}] ".format(datetime.datetime.now()), x)


def create_event_key(comp_code, quote_dt):
    dt = quote_dt[:13].replace("-", "").replace(" ", "")
    dt2 = quote_dt.replace("-", "").replace(" ", "").replace(":", "")
    return "events/date={}/{}_{}.json".format(dt, comp_code, dt2)


def run_batch_job(job_name, queue_name, asynch=False, env={}):
    batch = boto3.client("batch")
    env = [{"name": x, "value": env[x]} for x in env]
    res = batch.submit_job(
        jobName=job_name,
        jobQueue=queue_name,
        jobDefinition=job_name,
        containerOverrides={"environment": env},
    )
    job_id = res["jobId"]
    for _ in range(12):
        res = batch.describe_jobs(jobs=[job_id])
        job_status = res["jobs"][0]["status"]
        if job_status in ["SUCCEEDED", "FAILED"] or asynch:
            break
        time.sleep(60)
    return {"job_status": job_status}


def scan(table, filter, limit=None):
    items = []
    res = table.scan(FilterExpression=filter)
    while "LastEvaluatedKey" in res and (limit is None or len(items) < limit):
        res = table.scan(
            FilterExpression=filter, ExclusiveStartKey=res["LastEvaluatedKey"]
        )
        items.extend(res["Items"])
    return items


def get_start_dt(event_table, start_dt=None):
    if start_dt is None:
        start_dt = datetime.datetime.now()
        start_dt -= datetime.timedelta(days=120)
        start_dt = start_dt.strftime(DB_DATE_FORMAT)
    quote_dt = None
    res = event_table.scan(
        FilterExpression=Attr("quote_dt").gt(start_dt),
        ProjectionExpression="comp_code,quote_dt",
    )
    for item in res["Items"]:
        if quote_dt is None or item["quote_dt"] < quote_dt:
            quote_dt = item["quote_dt"]
    while "LastEvaluatedKey" in res:
        filter = Attr("quote_dt").gt(start_dt)
        if quote_dt is not None:
            filter = filter & Attr("quote_dt").lt(quote_dt)
        res = event_table.scan(
            FilterExpression=filter,
            ExclusiveStartKey=res["LastEvaluatedKey"],
            ProjectionExpression="comp_code,quote_dt",
        )
        for item in res["Items"]:
            if quote_dt is None or item["quote_dt"] < quote_dt:
                quote_dt = item["quote_dt"]
    return quote_dt


def list_companies(event_table):
    print("list companies")
    comp_codes = {}
    quote_dts = {}
    start_dt = get_start_dt(event_table)
    if start_dt is None:
        return comp_codes
    res = event_table.scan(
        FilterExpression=Attr("quote_dt").gte(start_dt),
        ProjectionExpression="comp_code,quote_dt",
    )
    for item in res["Items"]:
        comp_code = item["comp_code"]
        quote_dt = item["quote_dt"]
        comp_codes[comp_code] = 1
        quote_dts[quote_dt] = 1
    while "LastEvaluatedKey" in res:
        res = event_table.scan(
            FilterExpression=Attr("quote_dt").gte(start_dt),
            ExclusiveStartKey=res["LastEvaluatedKey"],
            ProjectionExpression="comp_code,quote_dt",
        )
        for item in res["Items"]:
            comp_code = item["comp_code"]
            quote_dt = item["quote_dt"]
            comp_codes[comp_code] = 1
            quote_dts[quote_dt] = 1
    quote_dts = list(quote_dts)
    quote_dts.sort()
    return list(comp_codes), quote_dts


def list_events(event_table):
    comp_codes = []
    quote_dts = []
    start_dt = get_start_dt(event_table)
    if start_dt is None:
        return comp_codes
    res = event_table.scan(
        FilterExpression=Attr("quote_dt").gte(start_dt),
        ProjectionExpression="comp_code,quote_dt",
    )
    for item in res["Items"]:
        comp_code = item["comp_code"]
        quote_dt = item["quote_dt"]
        comp_codes.append(comp_code)
        quote_dts.append(quote_dt)
    while "LastEvaluatedKey" in res:
        res = event_table.scan(
            FilterExpression=Attr("quote_dt").gte(start_dt),
            ExclusiveStartKey=res["LastEvaluatedKey"],
            ProjectionExpression="comp_code,quote_dt",
        )
        for item in res["Items"]:
            comp_code = item["comp_code"]
            quote_dt = item["quote_dt"]
            comp_codes.append(comp_code)
            quote_dts.append(quote_dt)
    keys = np.argsort(quote_dts)
    quote_dts = [quote_dts[i] for i in keys]
    comp_codes = [comp_codes[i] for i in keys]
    return comp_codes, quote_dts


class Discretizer:
    def __init__(self, bucket=None, discretizer=None):
        if discretizer is None:
            obj_key = "model/discretizer.pickle"
            f = bucket.Object(obj_key).get()
            discretizer = f["Body"].read()
            self.discretizer = pickle.loads(discretizer)
        else:
            self.discretizer = discretizer
        self.n_bins = self.discretizer.n_bins_[0]
        self.bins = self.discretizer.bin_edges_[0][1:-1]

    def save(self, bucket):
        discretizer = pickle.dumps(self.discretizer)
        obj_key = "model/discretizer.pickle"
        bucket.put_object(Key=obj_key, Body=discretizer)

    def price_class(self, price_ch):
        for edgei, edge in enumerate(self.bins):
            if price_ch < edge:
                return edgei
        return self.n_bins - 1


class OldEvent:
    def __init__(
        self, event=None, bucket=None, event_table=None, comp_code=None, quote_dt=None
    ):
        self.source_file = ""
        persist = False
        if event is None:
            print("old event")
            res = event_table.query(
                KeyConditionExpression=Key("comp_code").eq(comp_code)
                & Key("quote_dt").eq(quote_dt)
            )
            if res["Items"]:
                self.source_file = res["Items"][0]["source_file"]
            if (
                not res["Items"]
                or "vals" not in res["Items"][0]
                or len(res["Items"][0]["vals"]) < 10
            ):
                obj_key = create_event_key(comp_code, quote_dt)
                f = bucket.Object(obj_key).get()
                event = f["Body"].read().decode("utf-8")
                event = json.loads(event)
                persist = True
            else:
                event = res["Items"][0]["vals"]
                event["comp_code"] = comp_code
                event["quote_dt"] = quote_dt
        if "high_price" not in event:
            event["high_price"] = event["price"]
        if "low_price" not in event:
            event["low_price"] = event["price"]
        for key in event.keys():
            if (
                key.startswith("price")
                or key.startswith("high")
                or key.startswith("low")
                or key.startswith("jump")
                or key == "scale"
            ):
                event[key] = float(event[key])
        if "scale" not in event:
            event["scale"] = 1
        for exp in range(1, 11):
            period = 2 ** exp
            key = f"price{period}"
            if key not in event:
                event[key] = event["price"]
            key = f"high{period}"
            if key not in event:
                event[key] = event["high_price"]
            key = f"low{period}"
            if key not in event:
                event[key] = event["low_price"]
            key = f"jumpup{period}"
            if key not in event:
                event[key] = event["price"] - event["low_price"]
            key = f"jumpdown{period}"
            if key not in event:
                event[key] = event["price"] - event["high_price"]
        hour = int(event["quote_dt"][11:13])
        if "start" not in event:
            event["start"] = 1 if hour <= 9 else 0
        if "end" not in event:
            event["end"] = 1 if hour >= 16 else 0
        for th in [10, 20, 40, 80]:
            key = f"ch>{th}"
            if key not in event:
                event[key] = 0
        self.event = event
        self.event_table = event_table
        if persist and self.event_table is not None:
            self.persist(self.event_table)

    def persist_types(self, x):
        if isinstance(x, float):
            return Decimal(str(round(x, 37)))
        return x

    def persist(self, event_table):
        vals = {
            x: self.persist_types(self.event[x])
            for x in self.event
            if x not in ["comp_code", "quote_dt"]
        }
        event_table.put_item(
            Item={
                "comp_code": self.event["comp_code"],
                "quote_dt": self.event["quote_dt"],
                "source_file": self.source_file,
                "vals": vals,
            }
        )

    def get_price(self):
        return float(self.event["price"]) * float(self.event["scale"])

    def get_high_price(self):
        return float(self.event["high_price"]) * float(self.event["scale"])

    def get_low_price(self):
        return float(self.event["low_price"]) * float(self.event["scale"])

    def get_inputs(self):
        flags = ["start", "end", "ch>10", "ch>20", "ch>40", "ch>80"]
        attrs = flags + ["low_price", "high_price"]
        period_attrs = ["price", "high", "low", "jumpup", "jumpdown"]
        for exp in range(1, 10):
            period = 2 ** exp
            for attr in period_attrs:
                attrs.append(f"{attr}{period}")
        price = float(self.event["price"])
        inputs = [
            float(self.event[x]) if x in flags else float(self.event[x]) / price
            for x in attrs
        ]
        hour = float(self.event["quote_dt"][11:13])
        inputs.append(hour)
        return inputs[6:14]

    def next(self, price, high_price, low_price, quote_dt):
        event = self.event.copy()
        prev_event = self.event
        event["price"] = price
        event["high_price"] = high_price
        event["low_price"] = low_price
        event["quote_dt"] = quote_dt
        scale = prev_event["scale"]
        ch = price / prev_event["price"] - 1
        for th in [10, 20, 40, 80]:
            event[f"ch>{th}"] = 1 if ch > th / 100 or ch < 1 / (th / 100 + 1) - 1 else 0
        if event["ch>80"]:
            scale *= prev_event["price"] / price
        event["scale"] = scale
        price *= scale
        for exp in range(1, 11):
            period = 2 ** exp
            key = f"price{period}"
            event[key] = price / period + prev_event[key] * (1 - 1 / period)
            key = f"high{period}"
            event[key] = max(
                high_price, high_price / period + prev_event[key] * (1 - 1 / period)
            )
            key = f"low{period}"
            event[key] = min(
                low_price, low_price / period + prev_event[key] * (1 - 1 / period)
            )
            key = f"jumpup{period}"
            key_low = f"low{period}"
            jumpup = price - prev_event[key_low]
            event[key] = max(jumpup, prev_event[key] * (1 - 1 / period))
            key = f"jumpdown{period}"
            key_high = f"high{period}"
            jumpdown = price - prev_event[key_high]
            event[key] = min(jumpdown, prev_event[key] * (1 - 1 / period))
        if np.isnan(event["price2"]):
            print(self.event["price"], prev_event["price2"])
            print(price)
        hour = int(quote_dt[11:13])
        event["start"] = 1 if hour <= 9 else 0
        event["end"] = 1 if hour >= 16 else 0
        return OldEvent(event)

    def save(self, bucket, obj_key):
        event = json.dumps(self.event)
        bucket.put_object(Key=obj_key, Body=bytearray(event, "utf-8"))


class Event:
    INPUT_SIZE = 20
    INPUT_SHAPE = (INPUT_SIZE, 2)

    def __init__(self, event=None, prev_event=None):
        self.source_file = ""
        self.event = event
        self.just_bought = False
        self.just_sold = False
        if prev_event:
            self.price_seq = prev_event.price_seq
            self.bought_seq = prev_event.bought_seq
            bought = int(
                (prev_event.bought_seq[-1] and not prev_event.just_sold)
                or prev_event.just_bought
            )
        else:
            self.price_seq = []
            self.bought_seq = []
            bought = 0
        self.price_seq.append(self.get_price())
        self.bought_seq.append(bought)
        # n = round(sum(math.pow(2, math.floor(i / 5)) for i in range(self.INPUT_SIZE)))
        # if len(self.price_seq) > n:
        #    del self.price_seq[0]
        #    del self.bought_seq[0]

    def get_price(self):
        return float(self.event["price"])

    def get_high_price(self):
        return float(self.event["high_price"])

    def get_low_price(self):
        return float(self.event["low_price"])

    def get_inputs(self):
        n = round(sum(math.pow(2, math.floor(i / 5)) for i in range(self.INPUT_SIZE)))
        price_seq = self.price_seq[-n:]
        filler = price_seq[0] if price_seq else 1
        seq = [filler] * max(0, n - len(price_seq)) + price_seq
        seq = np.flip(seq)
        seq2 = []
        seq_i = 0
        for i in range(self.INPUT_SIZE):
            next_i = round(seq_i + math.pow(2, math.floor(i / 5)))
            seq2.append(np.mean(seq[seq_i:next_i]))
            seq_i = next_i
        seq = np.flip(seq2)
        seq = [(x / seq[-1] - 1) * 100 for x in seq]
        bought_seq = self.bought_seq[-self.INPUT_SIZE :]
        bought_seq = [0] * max(0, self.INPUT_SIZE - len(bought_seq)) + bought_seq
        inp = list(zip(seq, bought_seq))
        return np.reshape(inp, np.shape(inp)).tolist()

    def _get_inputs(self):
        n = self.INPUT_SIZE
        price_seq = self.price_seq[-n:]
        filler = price_seq[0] if price_seq else 1
        seq = [filler] * max(0, n - len(price_seq)) + price_seq
        seq = [(x / seq[-1] - 1) * 100 for x in seq]
        bought_seq = self.bought_seq[-n:]
        bought_seq = [0] * max(0, n - len(bought_seq)) + bought_seq
        inp = list(zip(seq, bought_seq))
        return np.reshape(inp, np.shape(inp)).tolist()

    def next(self, price, quote_dt):
        event = self.event.copy()
        event["price"] = price
        event["quote_dt"] = quote_dt
        return Event(event, prev_event=self)


class PriceChModelBase:
    def load_model(self, bucket=None, model=None):
        if model is None:
            f = bucket.Object(self.obj_key).get()
            model = f["Body"].read()
            self.model = pickle.loads(model)
        else:
            self.model = model

    def scale_init(self):
        input_size = len(
            OldEvent(
                {
                    "comp_code": "ABC",
                    "quote_dt": "2020-01-01 10:00:00",
                    "price": 1,
                    "high_price": 1,
                    "low_price": 1,
                }
            ).get_inputs()
        )
        if not hasattr(self.model, "scale_shift"):
            self.reset_params()
        else:
            self.scale_offset = (
                self.model.scale_offset
                if hasattr(self.model, "scale_offset")
                else 0.0001
            )
            self.scale_power = (
                self.model.scale_power if hasattr(self.model, "scale_power") else 1
            )
            self.scale_c_up = (
                self.model.scale_c_up if hasattr(self.model, "scale_c_up") else 15
            )
            self.scale_c_down = (
                self.model.scale_c_down if hasattr(self.model, "scale_c_down") else 15
            )
            self.scale_shift = (
                self.model.scale_shift if hasattr(self.model, "scale_shift") else 0
            )

        self.scale_offset_prev = (
            self.model.scale_offset_prev
            if hasattr(self.model, "scale_offset_prev")
            else self.scale_offset
        )
        self.scale_power_prev = (
            self.model.scale_power_prev
            if hasattr(self.model, "scale_power_prev")
            else self.scale_power
        )
        self.scale_c_up_prev = (
            self.model.scale_c_up_prev
            if hasattr(self.model, "scale_c_up_prev")
            else self.scale_c_up
        )
        self.scale_c_down_prev = (
            self.model.scale_c_down_prev
            if hasattr(self.model, "scale_c_down_prev")
            else self.scale_c_down
        )
        self.scale_shift_prev = (
            self.model.scale_shift_prev
            if hasattr(self.model, "scale_shift_prev")
            else self.scale_shift
        )

        self.scale_offset_ch = (
            self.model.scale_offset_ch if hasattr(self.model, "scale_offset_ch") else 0
        )
        self.scale_power_ch = (
            self.model.scale_power_ch if hasattr(self.model, "scale_power_ch") else 0
        )
        self.scale_c_up_ch = (
            self.model.scale_c_up_ch if hasattr(self.model, "scale_c_up_ch") else 0
        )
        self.scale_c_down_ch = (
            self.model.scale_c_down_ch if hasattr(self.model, "scale_c_down_ch") else 0
        )
        self.scale_shift_ch = (
            self.model.scale_shift_ch if hasattr(self.model, "scale_shift_ch") else 0
        )

        self.means = (
            self.model.means if hasattr(self.model, "means") else [0] * input_size
        )
        self.stds = self.model.stds if hasattr(self.model, "stds") else [1] * input_size

        # self.reset_params()

    def reset_params(self):
        self.scale_offset = 0.0001
        self.scale_power = 1
        self.scale_c_up = 1
        self.scale_c_down = 1
        self.scale_shift = -0.0032

        self.scale_offset_ch = 0
        self.scale_power_ch = 0
        self.scale_c_up_ch = 0
        self.scale_c_down_ch = 0
        self.scale_shift_ch = 0

    def save(self, bucket):
        self.model.scale_offset = self.scale_offset
        self.model.scale_power = self.scale_power
        self.model.scale_c_up = self.scale_c_up
        self.model.scale_c_down = self.scale_c_down
        self.model.scale_shift = self.scale_shift

        self.model.scale_offset_ch = self.scale_offset_ch
        self.model.scale_power_ch = self.scale_power_ch
        self.model.scale_c_up_ch = self.scale_c_up_ch
        self.model.scale_c_down_ch = self.scale_c_down_ch
        self.model.scale_shift_ch = self.scale_shift_ch

        self.model.scale_offset_prev = self.scale_offset_prev
        self.model.scale_power_prev = self.scale_power_prev
        self.model.scale_c_up_prev = self.scale_c_up_prev
        self.model.scale_c_down_prev = self.scale_c_down_prev
        self.model.scale_shift_prev = self.scale_shift_prev

        self.model.means = self.means
        self.model.stds = self.stds
        model = pickle.dumps(self.model)
        bucket.put_object(Key=self.obj_key, Body=model)

    def fit(self, train_x, train_y):
        train_x = (np.array(train_x) - self.means) / self.stds
        train_x = [x / (1 + abs(x)) for x in train_x]
        self.model.fit(train_x, train_y)

    def predict(self, test_x):
        test_x = (np.array(test_x) - self.means) / self.stds
        test_x = [x / (1 + abs(x)) for x in test_x]
        return self.model.predict([test_x])[0]

    def get_input_shape(self):
        return tuple([np.shape(self.model.coefs_[0])[0]])

    def adjust_ch(self, val, val_ch):
        if val is None:
            return val_ch, 0
        if val_ch * val > 0:
            val_ch *= 2
        elif val_ch * val < 0:
            val_ch /= 3
        elif val == 0:
            val_ch = 0
        else:
            val_ch = math.copysign(1, val)
        val_ch = min(1000, abs(val_ch))
        val_ch = math.copysign(val_ch, val)
        return val_ch, val

    def adjust(self, scale_offset, scale_power, scale_c_up, scale_c_down, scale_shift):
        # print("Current", self.scale_offset, self.scale_power, self.scale_c_up, self.scale_c_down, self.scale_shift)
        # print(
        #     "Momentum",
        #     self.scale_offset_ch,
        #     self.scale_power_ch,
        #     self.scale_c_up_ch,
        #     self.scale_c_down_ch,
        #     self.scale_shift_ch,
        # )
        # print("Change", scale_offset, scale_power, scale_c_up, scale_c_down, scale_shift)

        self.scale_offset_prev = self.scale_offset
        self.scale_power_prev = self.scale_power
        self.scale_c_up_prev = self.scale_c_up
        self.scale_c_down_prev = self.scale_c_down
        self.scale_shift_prev = self.scale_shift

        self.scale_offset_ch, scale_offset = self.adjust_ch(
            scale_offset, self.scale_offset_ch
        )
        self.scale_power_ch, scale_power = self.adjust_ch(
            scale_power, self.scale_power_ch
        )
        self.scale_c_up_ch, scale_c_up = self.adjust_ch(scale_c_up, self.scale_c_up_ch)
        self.scale_c_down_ch, scale_c_down = self.adjust_ch(
            scale_c_down, self.scale_c_down_ch
        )
        self.scale_shift_ch, scale_shift = self.adjust_ch(
            scale_shift, self.scale_shift_ch
        )

        if abs(scale_power * abs(self.scale_power_ch)) >= 1.5:
            self.scale_power_ch = math.copysign(1 / scale_power, self.scale_power_ch)

        self.scale_offset += scale_offset * 0.00001 * abs(self.scale_offset_ch)
        self.scale_power = min(
            5,
            max(1, round(self.scale_power + scale_power * abs(self.scale_power_ch), 1)),
        )
        self.scale_c_up = max(
            1e-5,
            self.scale_c_up + scale_c_up * abs(self.scale_c_up_ch),
        )
        self.scale_c_down = max(
            1e-5,
            self.scale_c_down + scale_c_down * abs(self.scale_c_down_ch),
        )
        self.scale_shift += scale_shift * 0.00001 * abs(self.scale_shift_ch)

        # print(
        #     "New momentum",
        #     self.scale_offset_ch,
        #     self.scale_power_ch,
        #     self.scale_c_up_ch,
        #     self.scale_c_down_ch,
        #     self.scale_shift_ch,
        # )
        # print("New", self.scale_offset, self.scale_power, self.scale_c_up, self.scale_c_down, self.scale_shift)
        self.save(self.bucket)

    def revert_adjustment(self):
        self.scale_offset = self.scale_offset_prev
        self.scale_power = self.scale_power_prev
        self.scale_c_up = self.scale_c_up_prev
        self.scale_c_down = self.scale_c_down_prev
        self.scale_shift = self.scale_shift_prev

        # print("Revert", self.scale_offset, self.scale_power, self.scale_c_up, self.scale_c_down, self.scale_shift)

        self.scale_offset_ch /= 8
        self.scale_power_ch /= 8
        self.scale_c_up_ch /= 8
        self.scale_c_down_ch /= 8
        self.scale_shift_ch /= 8

        # print(
        #     "Momentum",
        #     self.scale_offset_ch,
        #     self.scale_power_ch,
        #     self.scale_c_up_ch,
        #     self.scale_c_down_ch,
        #     self.scale_shift_ch,
        # )

        self.save(self.bucket)


class OldPriceChModel(PriceChModelBase):
    def __init__(self, bucket=None, model=None):
        self.bucket = bucket
        self.obj_key = "model/pricech_model2.pickle"
        self.load_model(bucket, model)
        self.scale_init()
        if bucket:
            self.discretizer = Discretizer(bucket)
            self.stats = [0] * self.discretizer.n_bins

    def predict(self, test_x):
        price_class = PriceChModelBase.predict(self, test_x)
        bins = self.discretizer.bins
        bini = np.digitize(price_class, bins)
        if bini == 0:
            price_ch = random.uniform(
                2 * bins[0] - bins[1], bins[0] * 0.6 + bins[1] * 0.4
            )
        elif bini >= len(bins):
            price_ch = random.uniform(
                bins[-1] * 0.6 + bins[-2] * 0.4, 2 * bins[-1] - bins[-2]
            )
        else:
            price_ch = random.uniform(
                bins[bini - 1] * 1.4 - bins[bini] * 0.4,
                bins[bini] * 1.4 - bins[bini - 1] * 0.4,
            )
        self.stats[bini] += 1
        return price_ch


class PriceChModel(PriceChModelBase):
    def __init__(self, bucket=None, model=None):
        self.bucket = bucket
        self.obj_key = "model/pricech_model.pickle"
        self.load_model(bucket, model)
        self.scale_init()


class Simulator:
    def __init__(self, bucket, offset=0, events=None):
        self.bucket = bucket
        static_init = False
        self.last_comp_code_i = -1
        if events is not None:
            static_init = True
            events2 = {}
            comp_codes = []
            quote_dt = events[0].event["quote_dt"]
            n_comps = len(events)
            for event in events:
                comp_code = event.event["comp_code"]
                vanishing = 1 if len(events) >= n_comps - N_VANISHING_COMPS else 0
                event.event["vanishing"] = vanishing
                events2[comp_code] = event
                comp_codes.append(comp_code)
            events = events2
        else:
            comp_codes = []
            n_comps = SIM_N_COMPS
            for _ in range(n_comps):
                comp_code = self.generate_comp_code()
                comp_codes.append(comp_code)
            quote_dt = "2020-01-01 09:30:00"
            events = {}
            for comp_code in comp_codes:
                price = self.generate_price()
                vanishing = 1 if len(events) >= SIM_N_COMPS - N_VANISHING_COMPS else 0
                event = {
                    "comp_code": comp_code,
                    "quote_dt": quote_dt,
                    "price": price,
                    "high_price": price,
                    "low_price": price,
                    "vanishing": vanishing,
                }
                events[comp_code] = Event(event)
        self.old_events = {}
        for comp_code in events:
            self.old_events[comp_code] = OldEvent(events[comp_code].event)
        self.comp_codes = comp_codes
        self.quote_dt = quote_dt
        self.events = events
        self.model = PriceChModel(bucket)
        self.overall = {}
        self.samples = {}
        self.offset = offset
        self.model.save(bucket)
        self.samples = {}
        # for comp_code in comp_codes[:10]:
        #    self.samples[comp_code] = [self.events[comp_code].get_price()]
        if not static_init:
            for _ in range(75):
                self.next()

    def generate_comp_code(self):
        self.last_comp_code_i += 1
        comp_code_i = self.last_comp_code_i
        nchar = 26
        start = 65
        c1 = chr(int(comp_code_i / (nchar * nchar)) + start)
        comp_code_i = comp_code_i % (nchar * nchar)
        c2 = chr(int(comp_code_i / nchar) + start)
        c3 = chr(comp_code_i % nchar + start)
        return c1 + c2 + c3

    def generate_price(self):
        price = 0
        while price <= 1 or price > 2500:
            price = math.pow(max(0.3, random.gauss(0.7, 0.1)), 6) * 1000
        return round(price, 2)

    def adjust(self, scale_offset, scale_power, scale_c_up, scale_c_down, scale_shift):
        self.model.adjust(
            scale_offset, scale_power, scale_c_up, scale_c_down, scale_shift
        )

    def revert_adjustment(self):
        self.model.revert_adjustment()

    def next(self):
        hour = int(self.quote_dt[11:13])
        quote_dt = datetime.datetime.strptime(self.quote_dt, DB_DATE_FORMAT)
        quote_dt += datetime.timedelta(hours=1)
        if quote_dt.hour > 16:
            quote_dt += datetime.timedelta(hours=17)
        if quote_dt.weekday() > 4:
            quote_dt += datetime.timedelta(days=2)
        renamed = {}
        hour = quote_dt.hour
        quote_dt = quote_dt.strftime(DB_DATE_FORMAT)
        events = {}
        old_events = {}
        base_ch = None
        for comp_code in self.comp_codes:
            inputs = self.old_events[comp_code].get_inputs()
            try:
                price_ch = self.model.predict(inputs)
            except:
                self.revert_adjustment()
            price_ch = 2 * (price_ch + random.gauss(0.001, 0.01))
            price_ch += self.model.scale_shift
            scale_c = self.model.scale_c_up if price_ch > 0 else self.model.scale_c_down
            price_ch = math.copysign(
                pow(
                    min(abs(price_ch) * scale_c, 0.9),
                    self.model.scale_power,
                ),
                price_ch,
            )
            price_ch += self.offset * self.model.scale_offset
            if base_ch is None:
                base_ch = price_ch / 2
            else:
                price_ch += base_ch
            price_ch = price_ch / (1 + 1 * abs(price_ch))
            price_ch = math.copysign(math.log(1 + abs(price_ch) * 100) / 100, price_ch)
            if "overall_c" in self.events[comp_code].event:
                overall_c = self.events[comp_code].event["overall_c"]
            else:
                overall_c = min(100, max(10, random.gauss(60, 15)))
                self.events[comp_code].event["overall_c"] = overall_c
            if comp_code in self.overall:
                c = overall_c
                stabilizers = [[10, 2.5 * c], [70, 3.5 * c], [1000, 1]]
                for n, c in stabilizers:
                    price_ch -= math.copysign(
                        np.average(self.overall[comp_code][-n:]) ** 2 * c,
                        np.average(self.overall[comp_code][-n:]) * c,
                    )
            if abs(price_ch) > 0.099:
                price_ch = math.copysign(0.099, price_ch)

            if not random.randrange(10):
                price_ch += random.gauss(0, 0.024 * (70 - overall_c) / 20)

            if not random.randrange(100):
                price_ch += random.gauss(0, 0.02)

            if "potential_ch" in self.events[comp_code].event:
                potential_ch = self.events[comp_code].event["potential_ch"]
            else:
                potential_ch = 0
            if not random.randrange(1000):
                potential_ch += random.gauss(0.05, 0.3)
            else:
                potential_ch *= 0.95
            self.events[comp_code].event["potential_ch"] = potential_ch
            if not random.randrange(3):
                price_ch += potential_ch * 0.1

            if "price_ch" in self.events[comp_code].event:
                prev_price_ch = self.events[comp_code].event["price_ch"]
            else:
                prev_price_ch = 0
            self.events[comp_code].event["price_ch"] = price_ch
            price_ch += prev_price_ch * 0.2

            price = self.events[comp_code].event["price"] * (price_ch + 1)
            price = max(0.01, round(price, 2))
            high_price = price * (
                1 + (hour - 10) / max(100, 1000 + random.gauss(0, 500))
            )
            high_price = max(price, round(high_price, 2))
            low_price = price * (
                1 - (hour - 10) / max(100, 1000 + random.gauss(0, 500))
            )
            low_price = min(price, max(0.01, round(low_price, 2)))
            events[comp_code] = self.events[comp_code].next(price, quote_dt)
            old_events[comp_code] = self.old_events[comp_code].next(
                price, high_price, low_price, quote_dt
            )
            if comp_code in renamed:
                events[comp_code].event["old_comp_code"] = renamed[comp_code]
            if comp_code in self.overall:
                self.overall[comp_code].append(price_ch)
            else:
                self.overall[comp_code] = [price_ch]
            if events[comp_code].bought_seq[-1]:
                self.samples[comp_code] = 1
        self.events = events
        self.old_events = old_events
        self.quote_dt = quote_dt
        # if hour == 16:
        #    for comp_code in self.samples:
        #        if comp_code in self.events:
        #            self.samples[comp_code].append(self.events[comp_code].get_price())
        batch = []
        for event in self.events.values():
            # vanishing = event.event["vanishing"]
            # add = random.randrange(7) if vanishing else 1
            # if add:
            batch.append(event)
        return batch

    def print_sample_quotes(self, comp_codes=None):
        for comp_code in self.samples:
            if comp_codes is None or comp_code in comp_codes:
                print(comp_code)
                print(self.samples[comp_code])

    def save_plots(self):
        plots = {}
        for comp_code in self.samples:
            plots[comp_code] = {
                "price_seq": self.events[comp_code].price_seq,
                "bought_seq": self.events[comp_code].bought_seq,
            }
        plots = json.dumps(plots)
        obj_key = "www/last_run_plots.json"
        self.bucket.put_object(Key=obj_key, Body=plots, ACL="public-read")


class HistSimulator:
    def __init__(self, db, event_table_name, bucket):
        self.dump_key = "extracts/all_hist.csv"
        try:
            obj = bucket.Object(self.dump_key)
            #if datetime.date.today() - obj.last_modified.date() > datetime.timedelta(7):
            #    raise Exception()
            self.hist = obj.get()["Body"].read().decode("utf-8")
            self.hist = self.hist.split("\n")
            self.hist_i = 0
        except:
            self.hist = None
            self.dump = ""
            self.db = db
            self.event_table_name = event_table_name
            self.event_table = db.Table(event_table_name)
            self.comp_codes, self.quote_dts = list_events(self.event_table)
            self.quote_dt_i = 0
            self.quote_dt = self.quote_dts[0]
        self.samples = {}
        self.events = {}
        self.bucket = bucket
        for _ in range(75):
            self.next()

    def get_events_from_db(self):
        events = {}
        for comp_code in self.events:
            self.events[comp_code].event["stale"] = True
        quote_dt = datetime.datetime.strptime(self.quote_dt, DB_DATE_FORMAT)
        quote_dt += datetime.timedelta(hours=1)
        quote_dt = quote_dt.strftime(DB_DATE_FORMAT)
        while (
            len(events) < 400
            and self.quote_dt_i < len(self.quote_dts)
            and self.quote_dt < quote_dt
        ):
            keys = []
            for _ in range(500):
                if self.quote_dt_i >= len(self.quote_dts):
                    break
                self.quote_dt = self.quote_dts[self.quote_dt_i]
                comp_code = self.comp_codes[self.quote_dt_i]
                self.quote_dt_i += 1
                keys.append({"comp_code": comp_code, "quote_dt": self.quote_dt})
            for batch_i in range(math.ceil(len(keys) / 10)):
                print("hist sim get batch")
                res = self.db.batch_get_item(
                    RequestItems={
                        self.event_table_name: {
                            "Keys": keys[batch_i * 10 : (batch_i + 1) * 10]
                        }
                    }
                )
                for item in res["Responses"][self.event_table_name]:
                    comp_code = item["comp_code"]
                    quote_dt = item["quote_dt"]
                    event = item["vals"]
                    event["comp_code"] = comp_code
                    event["quote_dt"] = quote_dt
                    event["stale"] = False
                    prev_event = (
                        self.events[comp_code] if comp_code in self.events else None
                    )
                    events[comp_code] = Event(event, prev_event)
        return events

    def get_events_from_file(self):
        events = self.events
        if self.hist_i + 2 >= len(self.hist):
            return {}
        self.quote_dt = self.hist[self.hist_i]
        comp_codes = self.hist[self.hist_i + 1].split(",")
        prices = self.hist[self.hist_i + 2].split(",")
        self.hist_i += 3
        for comp_code in events:
            events[comp_code].event["stale"] = True
            if comp_code not in comp_codes:
                comp_codes.append(comp_code)
                prices.append(events[comp_code].get_price())
        for comp_code, price in zip(comp_codes, prices):
            if not comp_code or not price:
                continue
            event = {}
            event["comp_code"] = comp_code
            event["quote_dt"] = self.quote_dt
            event["price"] = price
            event["stale"] = False
            prev_event = self.events[comp_code] if comp_code in self.events else None
            events[comp_code] = Event(event, prev_event)
        return events

    def save_dump(self):
        self.bucket.put_object(Key=self.dump_key, Body=bytearray(self.dump, "utf-8"))

    def next(self):
        self.prev_events = self.events.copy()
        if self.hist:
            self.events = self.get_events_from_file()
        else:
            events = self.get_events_from_db()
            if events:
                self.dump += self.quote_dt + "\n"
                self.dump += ",".join(events) + "\n"
                self.dump += (
                    ",".join([str(events[x].get_price()) for x in events]) + "\n"
                )
                for comp_code in self.events:
                    if comp_code not in events:
                        events[comp_code] = Event(
                            self.events[comp_code].event, self.events[comp_code]
                        )
                self.events = events
            else:
                self.save_dump()
                self.events = {}
        for comp_code in self.events:
            if self.events[comp_code].bought_seq[-1]:
                self.samples[comp_code] = 1
        if not self.events:
            return None
        # print(self.quote_dt, len(events))
        batch = list(self.events.values())
        # hour = int(self.quote_dt[11:13])
        # if hour == 16:
        #    if self.samples:
        #        # for comp_code in self.samples:
        #        for comp_code in self.events:
        #            if comp_code not in self.samples:
        #                self.samples[comp_code] = []
        #            self.samples[comp_code].append(self.events[comp_code].get_price())
        #    else:
        #        # for comp_code in list(self.events)[:10]:
        #        for comp_code in list(self.events):
        #            self.samples[comp_code] = [self.events[comp_code].get_price()]
        return batch

    def print_sample_quotes(self, filter_comps=None):
        for comp_code in self.samples:
            if filter_comps is None or comp_code in filter_comps:
                print(comp_code)
                print(self.samples[comp_code])

    def save_plots(self):
        plots = {}
        for comp_code in self.samples:
            if not comp_code in self.prev_events:
                continue
            plots[comp_code] = {
                "price_seq": self.prev_events[comp_code].price_seq,
                "bought_seq": self.prev_events[comp_code].bought_seq,
            }
        plots = json.dumps(plots)
        obj_key = "www/last_hist_plots.json"
        self.bucket.put_object(Key=obj_key, Body=plots, ACL="public-read")


class Settings:
    def __init__(self, bucket):
        self.path = "model/settings.json"
        try:
            f = bucket.Object(self.path).get()
            settings = f["Body"].read().decode("utf-8")
            settings = json.loads(settings)
        except:
            settings = {}
        self.map = settings
        self.bucket = bucket

    def save(self):
        settings = json.dumps(self.map)
        self.bucket.put_object(Key=self.path, Body=settings)
