import pytest
import myutils
import pythonjob.glue_utils as glue_utils
import boto3
import numpy as np

class TestPriceCh():

    @pytest.fixture(scope='class')
    def vars(self):
        vars = myutils.get_vars()
        job_name = vars['id'] + '_events'
        myutils.run_glue_job(job_name, {'--repeat': '20'})
        job_name = vars['id'] + '_pricech_model'
        res = myutils.run_glue_job(job_name)
        vars.update(res)
        return vars

    def test_status(self, vars):
        job_status = vars['job_status']
        assert job_status == 'SUCCEEDED'
    
    def test_model(self, vars):
        bucket_name = vars['bucket_name']
        s3 = boto3.resource('s3')
        bucket = s3.Bucket(bucket_name)
        event_table_name = vars['id'] + '_events'
        db = boto3.resource('dynamodb')
        event_table = db.Table(event_table_name)
        model = glue_utils.PriceChModel(bucket)
        res = event_table.scan()
        item = res['Items'][0]
        event = glue_utils.Event({'comp_code':item['comp_code'], 'quote_dt':item['quote_dt']})
        assert model.get_input_shape() == np.shape(event.get_inputs())
        price_ch = model.predict(event.get_inputs())
        assert 0 <= price_ch <= 1
        