import pytest
import myutils
import pythonjob.glue_utils as glue_utils
import boto3
import numpy as np
import random
import json


class TestSimulator:
    @pytest.fixture(scope="class")
    def vars(self):
        vars = myutils.get_vars()
        bucket_name = vars["bucket_name"]
        s3 = boto3.resource("s3")
        bucket = s3.Bucket(bucket_name)
        vars["simulator"] = glue_utils.Simulator(bucket)
        event_table_name = vars["id"] + "_events"
        db = boto3.resource("dynamodb")
        vars["hist_simulator"] = glue_utils.HistSimulator(db, event_table_name, bucket)
        return vars

    def test_events(self, vars):
        simulator = vars["simulator"]
        events = simulator.next()
        assert len(events) >= 0.9 * glue_utils.SIM_N_COMPS
        comp_codes = {}
        for event in events:
            comp_code = event.event["comp_code"]
            assert len(comp_code) == 3
            assert "AAA" <= comp_code <= "ZZZ"
            comp_codes[comp_code] = event
        assert len(comp_codes) >= 0.9 * glue_utils.SIM_N_COMPS
        events = simulator.next()
        n_same = sum(1 if x.event["comp_code"] in comp_codes else 0 for x in events)
        assert n_same > 0.9 * glue_utils.SIM_N_COMPS
        for event in events:
            comp_code = event.event["comp_code"]
            if comp_code not in comp_codes:
                continue
            price1 = comp_codes[comp_code].get_price()
            price2 = event.get_price()
            price_ch = price2 / price1 - 1
            assert -0.1 < price_ch < 0.1
        for comp_code in simulator.samples:
            simulator.samples[comp_code] = simulator.samples[comp_code][-1:]
        for _ in range(500):
            events = simulator.next()
        #self.compare_simulators(vars)
        
    def add_to_plots(self, plots, events, label):
        for event in events:
            comp_code = event.event["comp_code"]
            add = 1 if comp_code in plots else (0 if len(plots) > 10 else random.choices([0, 1], [30, 1])[0])
            if add:
                plot = plots[comp_code] if comp_code in plots else {}
                plot2 = plot[label] if label in plot else []
                plot2.append(event.get_price())
                plot[label] = plot2
                plots[comp_code] = plot

    def compare_simulators(self, vars):
        simulator = vars["simulator"]
        hist_simulator = vars["hist_simulator"]
        bucket_name = vars["bucket_name"]
        s3 = boto3.resource("s3")
        bucket = s3.Bucket(bucket_name)
        plots = {}

        for _ in range(200):
            events = hist_simulator.next()
            self.add_to_plots(plots, events, "hist")
            self.add_to_plots(plots, events, "rand")

        simulator = glue_utils.Simulator(bucket, events=events)

        for _ in range(200):
            events = hist_simulator.next()
            if events is None:
                break
            self.add_to_plots(plots, events, "hist")
            events = simulator.next()
            self.add_to_plots(plots, events, "rand")

        obj_key = "www/compare_plots.json"
        plots = json.dumps(plots)
        bucket.put_object(Key=obj_key, Body=plots, ACL="public-read")
