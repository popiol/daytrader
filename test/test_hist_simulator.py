import pytest
import myutils
import pythonjob.glue_utils as glue_utils
import boto3
import numpy as np


class TestHistSimulator:
    @pytest.fixture(scope="class")
    def vars(self):
        vars = myutils.get_vars()
        bucket_name = vars["bucket_name"]
        s3 = boto3.resource("s3")
        bucket = s3.Bucket(bucket_name)
        event_table_name = vars["id"] + "_events"
        db = boto3.resource("dynamodb")
        vars["simulator"] = glue_utils.HistSimulator(db, event_table_name, bucket)
        return vars

    def test_events(self, vars):
        bucket_name = vars["bucket_name"]
        s3 = boto3.resource("s3")
        bucket = s3.Bucket(bucket_name)
        event_table_name = vars["id"] + "_events"
        db = boto3.resource("dynamodb")
        simulator = vars["simulator"]
        events = simulator.next()
        assert len(events) >= 0.9 * glue_utils.SIM_N_COMPS
        comp_codes = {}
        for event in events:
            comp_code = event.event["comp_code"]
            assert 1 <= len(comp_code) <= 5
            assert "A" <= comp_code <= "ZZZZZ"
            comp_codes[comp_code] = event
        assert len(comp_codes) >= 0.9 * glue_utils.SIM_N_COMPS
        events = simulator.next()
        n_same = sum(1 if x.event["comp_code"] in comp_codes else 0 for x in events)
        assert n_same > 0.9 * glue_utils.SIM_N_COMPS
        for event in events:
            comp_code = event.event["comp_code"]
            if comp_code not in comp_codes:
                continue
            price1 = comp_codes[comp_code].get_price()
            price2 = event.get_price()
            price_ch = price2 / price1 - 1
            assert -0.1 < price_ch < 0.1
        while events is not None:
            events = simulator.next()
        # print(simulator.samples)
        # assert 1 == 0
